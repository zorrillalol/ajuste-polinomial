package com.example.laboratoriomaterias;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Looper;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.UUID;





public class Laboratorio_fisica extends AppCompatActivity {
    private BluetoothAdapter mBTAdapter;
    private ArrayList<Integer>  data;
    private String dataS="",dataS2="" ;
    Handler  bluetoothIn;
    final int handlerState = 0;
    private final static int MESSAGE_READ = 2;
    private BluetoothSocket socket = null;
    private StringBuilder DataStringIN = new StringBuilder();
    private ConnectedThread ConexionBT;
    public static String MIS_DATOS="DATA";
    // Identificador unico de servicio - SPP UUID
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    // String para la direccion MAC
    private static String address = null;
    private final static int CONNECTING_STATUS = 3;
    private Mensaje m = new Mensaje();
    public static ArrayList<Integer> EXTRA_DATA;
    public TextView txtD ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.laboratorio_fisica);
        txtD = findViewById(R.id.txtDatosRecibidos);

        Button btnGrafica = findViewById(R.id.btnGrafica);
        //Button btnConectarBT = findViewById(R.id.btnConectar);


        Button btnDeconectar = findViewById(R.id.btnDesconectar);
        final TextView txtDatos= findViewById(R.id.txtDatosRecibidos);
        btnGrafica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dataS!=""&&dataS2!=""){
                    dataS+="|"+dataS2;
                    Intent i = new Intent(Laboratorio_fisica.this, Grafica_activity.class);//<-<- PARTE A MODIFICAR >->->
                    i.putExtra("MIS_DATOS", dataS);
                    startActivity(i);
                }

            }
        });
        /*btnConectarBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Laboratorio_fisica.this,ListaDeDispositivos.class);
                startActivity(intent);

                //AcceptThread ac = new AcceptThread();
                //ac.run();
            }
        });*/
        //Codigo para recibir los datos



        mBTAdapter = BluetoothAdapter.getDefaultAdapter();
        VerificarEstadoBT();


        btnDeconectar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (socket!=null)
                {
                    try {socket.close();}
                    catch (IOException e)
                    { Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_SHORT).show();;}
                }
                Intent intent = new Intent(Laboratorio_fisica.this, Menu_Materia_Activity.class);
                startActivity(intent);
            }
        });
    }
    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException
    {
        //crea un conexion de salida segura para el dispositivo
        //usando el servicio UUID
        return device.createRfcommSocketToServiceRecord(BTMODULEUUID);
    }

    public  boolean isNumeric(String cadena) {

        boolean resultado;

        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }
    @Override
    public void onResume() {
        Button btnEncender = findViewById(R.id.btnEncender);
        Button btnApagar = findViewById(R.id.btnApagar);
        super.onResume();
        //Consigue la direccion MAC desde DeviceListActivity via intent
        Intent intent = getIntent();
        //Consigue la direccion MAC desde DeviceListActivity via EXTRA


        address = intent.getStringExtra(ListaDeDispositivos.EXTRA_DEVICE_ADDRESS);//<-<- PARTE A MODIFICAR >->->
        //Setea la direccion MAC
        txtD.setText("Address: " + address);

        new Thread() {
            public void run() {
                BluetoothDevice device = mBTAdapter.getRemoteDevice(address);

                try {
                    socket = createBluetoothSocket(device);
                } catch (IOException e) {
                    Toast.makeText(getBaseContext(), "La creacción del Socket fallo", Toast.LENGTH_LONG).show();
                }
                // Establece la conexión con el socket Bluetooth.
                try {
                    socket.connect();
                } catch (IOException e) {
                    try {
                        socket.close();
                    } catch (IOException e2) {
                    }
                }
                ConexionBT = new ConnectedThread(socket);

                ConexionBT.start();

                bluetoothIn.obtainMessage(CONNECTING_STATUS, 0, -1, "")
                        .sendToTarget();
                //ConexionBT.write("x".getBytes());
                //  Toast.makeText(getBaseContext(), "Empezo el envio de datos", Toast.LENGTH_LONG).show();
            }
        }.start();

        m.handler();
        btnEncender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView txt= findViewById(R.id.txtdatos2);
                if (ConexionBT != null){ //First check to make sure thread created
                    //ConexionBT.write("1")
                    if(dataS!=""){
                        txt.setText(dataS);
                    }
                }
               // txt.setText("ex");
        }
        });
        btnApagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cad="Hola";
                ConexionBT.write(cad);
               // m.setLeer(false);
               // data= m.getDatos();
            }
        });
    }
    private class Mensaje{
        private boolean leer = false;
        ArrayList<Integer> datos = new ArrayList<Integer>();
        public Mensaje(){

        }
        private void handler() {


            bluetoothIn =new Handler(Looper.getMainLooper()) {
                public void handleMessage(android.os.Message msg) {

                    if (msg.what == MESSAGE_READ) {
                        String readMessage = null;
                        try {
                            readMessage = new String((byte[]) msg.obj, "UTF-8");
                            /*if (readMessage.charAt(0)=='#') {
                                dataS = readMessage;
                                dataS= dataS.substring(1);
                            }else if(readMessage.charAt(0)  =='#'&& readMessage.charAt(1)=='#'){
                                dataS2=readMessage;
                                dataS2= dataS2.substring(2);

                            }*/
                            TextView txtDatos=findViewById(R.id.txtDatosRecibidos);
                            txtDatos.setText(readMessage);

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                        readMessage=null;
                    }
                }
            };

        }
        public void setLeer(boolean decision){
            leer=decision;
        }

        public ArrayList<Integer> getDatos() {
            return datos;
        }
    }


    private void VerificarEstadoBT() {

        if(mBTAdapter==null) {
            Toast.makeText(getBaseContext(), "El dispositivo no soporta bluetooth", Toast.LENGTH_LONG).show();
        } else {
            if (mBTAdapter.isEnabled()) {
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }
    private class ConnectedThread extends Thread
    {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket)
        {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            try
            {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            Thread thread = new Thread(new Runnable() {
                //byte[] buffer = new byte[1024];  // buffer store for the stream
                byte[] buffer = new byte[256];
                int bytes; // bytes returned from read()

                @Override
                public void run() {

                    while (true) {

                        try {
                            // Read from the InputStream
                            bytes = mmInStream.available();
                            if (bytes != 0) {
                                //                        SystemClock.sleep(100); //pause and wait for rest of data. Adjust this depending on your sending speed.
                                bytes = mmInStream.available(); // how many bytes are ready to be read?
                                bytes = mmInStream.read(buffer, 0, bytes); // record how many bytes we actually read
                                bluetoothIn.obtainMessage(MESSAGE_READ, bytes, -1, buffer)
                                        .sendToTarget(); // Send the obtained bytes to the UI activity
                                String mensaje = bluetoothIn.obtainMessage(MESSAGE_READ, bytes, -1, buffer)
                                        .toString(); // Send the obtained bytes to the UI activity
                                TextView txt = findViewById(R.id.txtdatos2);
                                txt.setText(mensaje);
                                buffer = null;
                            }
                            bytes=0;


                        } catch (IOException e) {
                            e.printStackTrace();

                            break;
                        }
                    }
                }
            });
            thread.start();
        }
        //Envio de trama
        public void write(String input) {
            byte[] bytes = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) { }
        }
    }

    public ArrayList<Integer> getData() {
        return data;
    }
}
