package com.example.laboratoriomaterias;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class Menu_Materia_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_materias);

        Button btnFisica = findViewById(R.id.btnFisica);

        btnFisica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Menu_Materia_Activity.this, ListaDeDispositivos.class);
                startActivity(intent);
            }
        });
    }
}
