package ultilerias;

public class AjustePolinomial {
    /**
     * Método que nos permite obtener la sumatoria de un arreglo a una potencia dada
     * @param arreglo Arreglo del cual queremos obtener la suma
     * @param potencia potencia deseada
     * @return sumatoria del arreglo con un valor double
     */
    public Double sumatoriaPotencia(Double[] arreglo,int potencia){
        Double valor=0.0;
        for (int i=0; i<arreglo.length;i++){
            valor+=Math.pow(arreglo[i],potencia);
        }
        return valor;
    }
}
